package com.hackaton.scheduledtransfersapi.utils;

public class Constants {

    public static final String BASE_URL = "/scheduletransfers/v1";
    public static final String DETAIL_TRANSFERS_URL = BASE_URL + "/detailtransfers";

    public static final String CAMPOS_INVALIDOS = "Campos inválidos";
    public static final String CLIENT_NOT_FOUND = "El cliente no existe";
    public static final String CHARGE_ACCOUNT_NOT_FOUND = "La cuenta de cargo no existe";
    public static final String CHARGE_ACCOUNT_NOT_FROM_CLIENT = "La cuenta de cargo no le pertenece al cliente";
    public static final String DESTINATION_ACCOUNT_NOT_FOUND = "La cuenta de destino no existe";
    public static final String DIFFERENT_CURRENCIES = "La moneda de la cuenta de cargo y de destino no coinciden";
    public static final String INSUFFICIENT_BALANCE = "La cuenta de cargo no tiene el saldo suficiente para programar la transferencia";

    public static final String INVALID_TOKEN = "Token de autentication inválido";

}
