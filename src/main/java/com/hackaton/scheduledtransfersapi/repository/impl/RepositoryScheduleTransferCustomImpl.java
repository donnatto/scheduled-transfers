package com.hackaton.scheduledtransfersapi.repository.impl;

import com.hackaton.scheduledtransfersapi.model.ScheduleTransferModify;
import com.hackaton.scheduledtransfersapi.repository.RepositoryScheduleTransferCustom;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class RepositoryScheduleTransferCustomImpl implements RepositoryScheduleTransferCustom {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void updateScheduleTransfer(String idScheduleTransfer, ScheduleTransferModify scheduleTransferModify) {

        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(new ObjectId(idScheduleTransfer)));
        final Update parche = new Update();
        if(scheduleTransferModify.status != null) parche.set("status", scheduleTransferModify.status);
        if(scheduleTransferModify.amount != null) parche.set("amount", scheduleTransferModify.amount);
        if(scheduleTransferModify.description != null) parche.set("description", scheduleTransferModify.description);
        if(scheduleTransferModify.destinationAccount != null) parche.set("destinationAccount", scheduleTransferModify.destinationAccount);
        this.mongoTemplate.updateFirst(filtro, parche, "ScheduleTransfer");
    }
}
