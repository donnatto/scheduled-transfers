package com.hackaton.scheduledtransfersapi.repository;

import com.hackaton.scheduledtransfersapi.model.ScheduleTransferModify;

public interface RepositoryScheduleTransferCustom {
    public void updateScheduleTransfer(String idScheduleTransfer, ScheduleTransferModify scheduleTransferModify);
}
