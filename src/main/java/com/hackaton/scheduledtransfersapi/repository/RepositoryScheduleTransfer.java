package com.hackaton.scheduledtransfersapi.repository;

import com.hackaton.scheduledtransfersapi.model.ScheduleTransfer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositoryScheduleTransfer extends MongoRepository<ScheduleTransfer, String>, RepositoryScheduleTransferCustom {

    public List<ScheduleTransfer> findByIdCustomer(String idCustomer);
    public ScheduleTransfer findByIdAndIdCustomer(String id, String customerId);
}
