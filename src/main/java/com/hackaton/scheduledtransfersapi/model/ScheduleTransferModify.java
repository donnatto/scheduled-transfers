package com.hackaton.scheduledtransfersapi.model;

public class ScheduleTransferModify {
    public String description;
    public String destinationAccount;
    public Double amount;
    public Boolean status;
}
