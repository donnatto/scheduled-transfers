package com.hackaton.scheduledtransfersapi.model;

import lombok.Data;

@Data
public class Account {

    private String accountId;
    private String customerId;
    private String accountNumber;
    private String accountType;
    private String currency;
    private Double balance;
    private String creationDate;
}
