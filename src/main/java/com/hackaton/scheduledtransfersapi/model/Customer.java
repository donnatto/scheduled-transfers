package com.hackaton.scheduledtransfersapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer implements Serializable {
    public String customerId;
    public String firstName;
    public String lastName;
    public String documentNumber;
    public String documentType;
    public String creationDate;

}
