package com.hackaton.scheduledtransfersapi.service;

import com.hackaton.scheduledtransfersapi.model.Customer;

import java.util.List;

public interface ServiceCustomer {
    boolean existsCustomer(String idCustomer);
}
