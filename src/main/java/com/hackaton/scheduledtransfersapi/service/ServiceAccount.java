package com.hackaton.scheduledtransfersapi.service;

import com.hackaton.scheduledtransfersapi.model.Account;

public interface ServiceAccount {
    boolean existsAccount(String idAccount);
    Account getAccount(String accountId);
}
