package com.hackaton.scheduledtransfersapi.service.impl;

import com.hackaton.scheduledtransfersapi.model.Customer;
import com.hackaton.scheduledtransfersapi.model.ScheduleTransfer;
import com.hackaton.scheduledtransfersapi.model.ScheduleTransferModify;
import com.hackaton.scheduledtransfersapi.repository.RepositoryScheduleTransfer;
import com.hackaton.scheduledtransfersapi.service.ServiceScheduleTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ServiceScheduleTransferImpl implements ServiceScheduleTransfer {
    @Autowired
    RepositoryScheduleTransfer repositoryScheduleTransfer;

    @Autowired
    RestTemplate restTemplate;
    @Value("${api.customer.v1}")
    String customerApiUrl;

    @Override
    public ScheduleTransfer getDetailScheduleTransfer(String idScheduleTransfer, String idCustomer){
        Map<String, String> params = new HashMap<String, String>();
        params.put("idCustomer", idCustomer);
        ResponseEntity<Customer> c = restTemplate.getForEntity(customerApiUrl, Customer.class, params);
        ScheduleTransfer detailByIdAndIdCustomer = null;
        if(c.getStatusCode() == HttpStatus.ACCEPTED){
            detailByIdAndIdCustomer =  repositoryScheduleTransfer.findByIdAndIdCustomer(idScheduleTransfer, idCustomer);
        }
        return detailByIdAndIdCustomer;
    }

    @Override
    public ScheduleTransfer save(ScheduleTransfer t) {
        return repositoryScheduleTransfer.save(t);
    }

    @Override
    public List<ScheduleTransfer> getAllScheduledTransfers() {
        return repositoryScheduleTransfer.findAll();
    }

    @Override
    public ScheduleTransfer getScheduleTransferById(String id) {
        return repositoryScheduleTransfer.findById(id).orElse(null);
    }

    @Override
    public void updateScheduleTransfer(ScheduleTransfer transfer, String id) {
        transfer.setId(id);
        repositoryScheduleTransfer.save(transfer);
    }

    @Override
    public void modifyScheduleTransfer(ScheduleTransfer transfer, String id) {
        ScheduleTransfer scheduleTransfer = repositoryScheduleTransfer.findById(id).orElse(null);
        if (scheduleTransfer == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        String idCustomer = transfer.getIdCustomer();
        String description = transfer.getDescription();
        String chargeAccount = transfer.getChargeAccount();
        Double amount = transfer.getAmount();

        if (idCustomer != null) {
            if (idCustomer.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            scheduleTransfer.setIdCustomer(idCustomer);
        }
        if (description != null) {
            if (description.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            scheduleTransfer.setDescription(description);
        }
        if (chargeAccount != null) {
            if (chargeAccount.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            scheduleTransfer.setChargeAccount(chargeAccount);
        }
        if (amount != null) {
            scheduleTransfer.setAmount(amount);
        }

        repositoryScheduleTransfer.save(scheduleTransfer);
    }


    @Override
    public void deleteScheduleTransfer(String id) { repositoryScheduleTransfer.deleteById(id);}

    @Override
    public List<ScheduleTransfer> getScheduleTransferByIdCustomer(String idCustomer) {
        return repositoryScheduleTransfer.findByIdCustomer(idCustomer);
    }

    @Override
    public void patchScheduleTransfer(String id, ScheduleTransferModify scheduleTransferModify) {
        repositoryScheduleTransfer.updateScheduleTransfer(id, scheduleTransferModify);
    }
}
