package com.hackaton.scheduledtransfersapi.service.impl;

import com.hackaton.scheduledtransfersapi.model.Customer;
import com.hackaton.scheduledtransfersapi.service.ServiceCustomer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class ServiceCustomerImpl implements ServiceCustomer {
    @Autowired
    RestTemplate restTemplate;
    @Value("${api.customer.v1}")
    String customerApiUrl;

    @Override
    public boolean existsCustomer(String idCustomer) {
        boolean exists = false;
        try {
            Map<String, String> params = new HashMap<>();
            params.put("idCustomer", idCustomer);
            ResponseEntity<Customer> r = restTemplate.getForEntity(customerApiUrl, Customer.class, params);
            if(r.getStatusCode() != HttpStatus.NOT_FOUND)
                exists = r.getBody() != null;
        }catch (RestClientException e){
            log.error(e.getMessage());
        }
        return exists;
    }

}
