package com.hackaton.scheduledtransfersapi.service.impl;

import com.hackaton.scheduledtransfersapi.model.Account;
import com.hackaton.scheduledtransfersapi.model.Customer;
import com.hackaton.scheduledtransfersapi.service.ServiceAccount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
public class ServiceAccountImpl implements ServiceAccount {

    @Autowired
    RestTemplate restTemplate;
    @Value("${api.account.v1}")
    String accountApiUrl;

    @Override
    public boolean existsAccount(String idAccount) {
        boolean exists = false;
        try {
            Map<String, String> params = new HashMap<>();
            params.put("idAccount", idAccount);
            ResponseEntity<Account> r = restTemplate.getForEntity(accountApiUrl, Account.class, params);
            if(r.getStatusCode() != HttpStatus.NOT_FOUND)
                exists = r.getBody() != null;
        }catch (RestClientException e){
            log.error(e.getMessage());
        }
        return exists;
    }

    @Override
    public Account getAccount(String accountId) {
        Map<String, String> params = new HashMap<>();
        params.put("idAccount", accountId);
        ResponseEntity<Account> account = restTemplate.getForEntity(accountApiUrl, Account.class, params);
        return account.getBody();
    }
}
