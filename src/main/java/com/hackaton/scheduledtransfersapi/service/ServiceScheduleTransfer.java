package com.hackaton.scheduledtransfersapi.service;


import com.hackaton.scheduledtransfersapi.model.ScheduleTransfer;
import com.hackaton.scheduledtransfersapi.model.ScheduleTransferModify;

import java.util.List;

public interface ServiceScheduleTransfer {
    ScheduleTransfer getDetailScheduleTransfer(String idScheduleTransfer, String idCustomer);
    ScheduleTransfer save(ScheduleTransfer t);
    List<ScheduleTransfer> getAllScheduledTransfers();
    ScheduleTransfer getScheduleTransferById(String id);
    void updateScheduleTransfer(ScheduleTransfer customer, String id);
    void modifyScheduleTransfer(ScheduleTransfer customer, String id);
    void deleteScheduleTransfer(String id);
    List<ScheduleTransfer> getScheduleTransferByIdCustomer(String idCustomer);
    void patchScheduleTransfer(String id, ScheduleTransferModify scheduleTransferModify);
}
