package com.hackaton.scheduledtransfersapi.controller;

import com.hackaton.scheduledtransfersapi.model.Account;
import com.hackaton.scheduledtransfersapi.model.ScheduleTransfer;
import com.hackaton.scheduledtransfersapi.model.ScheduleTransferModify;
import com.hackaton.scheduledtransfersapi.security.JwtBuilder;
import com.hackaton.scheduledtransfersapi.service.ServiceAccount;
import com.hackaton.scheduledtransfersapi.service.ServiceCustomer;
import com.hackaton.scheduledtransfersapi.service.ServiceScheduleTransfer;
import com.hackaton.scheduledtransfersapi.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.jose4j.jwt.JwtClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("${api.v1}")
public class ControllerScheduleTransfer {

    @Autowired
    ServiceCustomer serviceCustomer;

    @Autowired
    ServiceScheduleTransfer serviceScheduleTransfer;

    @Autowired
    ServiceAccount serviceAccount;


    @Autowired
    JwtBuilder jwtBuilder;

    @GetMapping("/transfers")
    public ResponseEntity<List<ScheduleTransfer>> getAllScheduledTransfers(
            @RequestHeader("Authorization") String authorization
    ) {
        validarToken(authorization);
        List<ScheduleTransfer> allCustomers = serviceScheduleTransfer.getAllScheduledTransfers();
        return (allCustomers != null && !allCustomers.isEmpty()) ?
                ResponseEntity.ok(allCustomers) :
                new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/transfers/{transferId}")
    public ResponseEntity<ScheduleTransfer> getScheduledTransferById(
            @RequestHeader("Authorization") String authorization,
            @PathVariable String transferId) {
        validarToken(authorization);
        ScheduleTransfer transferById = serviceScheduleTransfer.getScheduleTransferById(transferId);
        return transferById != null ? ResponseEntity.ok(transferById) : ResponseEntity.noContent().build();
    }

    @GetMapping("/customers/{idCustomer}")
    public ResponseEntity<List<ScheduleTransfer>> listScheduleTransfer(
            @RequestHeader("Authorization") String authorization,
            @PathVariable String idCustomer){
        validarToken(authorization);
        if(!serviceCustomer.existsCustomer(idCustomer))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(serviceScheduleTransfer.getScheduleTransferByIdCustomer(idCustomer));
    }

    @GetMapping("/customers/{idCustomer}/{idScheduleTransfer}")
    public ResponseEntity <ScheduleTransfer> getDetailScheduleTransferByIdCustomer(
            @RequestHeader("Authorization") String authorization,
            @PathVariable String idScheduleTransfer,
            @PathVariable String idCustomer){
        validarToken(authorization);
        if(serviceScheduleTransfer.getDetailScheduleTransfer(idScheduleTransfer, idCustomer) == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else return ResponseEntity.ok(serviceScheduleTransfer.getDetailScheduleTransfer(idScheduleTransfer, idCustomer));
    }

    @PostMapping("/transfers")
    public ResponseEntity<ScheduleTransfer> saveScheduleTransfer(
            @RequestHeader("Authorization") String authorization,
            @RequestBody ScheduleTransfer scheduleTransfer){
        validarToken(authorization);
        String idCustomer = scheduleTransfer.getIdCustomer();
        String chargeAccountId = scheduleTransfer.getChargeAccount();
        String destinationAccountId = scheduleTransfer.getDestinationAccount();
        Double amount = scheduleTransfer.getAmount();
        preValidation(idCustomer, chargeAccountId, destinationAccountId, amount);
        return ResponseEntity.ok(serviceScheduleTransfer.save(scheduleTransfer));
    }

    @PutMapping("/transfers/{transferId}")
    public ResponseEntity<Void> updateScheduleTransfer(
            @RequestHeader("Authorization") String authorization,
            @RequestBody ScheduleTransfer transfer,
            @PathVariable String transferId) {
        validarToken(authorization);
        if (isValid(transferId))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id de transferencia inválido");
        ScheduleTransfer foundTransfer = serviceScheduleTransfer.getScheduleTransferById(transferId);
        if (foundTransfer == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "La transferencia no existe");
        
        String idCustomer = transfer.getIdCustomer();
        String chargeAccountId = transfer.getChargeAccount();
        String destinationAccountId = transfer.getDestinationAccount();
        Double amount = transfer.getAmount();
        preValidation(idCustomer, chargeAccountId, destinationAccountId, amount);

        serviceScheduleTransfer.updateScheduleTransfer(transfer, transferId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/transfers/{idScheduleTransfer}")
    public ResponseEntity<Void> deleteScheduleTransfer(
            @RequestHeader("Authorization") String authorization,
            @PathVariable String idScheduleTransfer) {
        validarToken(authorization);
        if (serviceScheduleTransfer.getScheduleTransferById(idScheduleTransfer) == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        serviceScheduleTransfer.deleteScheduleTransfer(idScheduleTransfer);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/transfers/{idScheduleTransfer}")
    public void modificatProducto(
            @RequestHeader("Authorization") String authorization,
            @PathVariable String idScheduleTransfer,
            @RequestBody ScheduleTransferModify scheduleTransferModify){
        validarToken(authorization);
        if(Objects.nonNull(scheduleTransferModify.destinationAccount) && !scheduleTransferModify.destinationAccount.trim().isEmpty()){
            if(!serviceAccount.existsAccount(scheduleTransferModify.destinationAccount))
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        this.serviceScheduleTransfer.patchScheduleTransfer(idScheduleTransfer, scheduleTransferModify);
    }

    private void validarToken(String authorization) {

        authorization = authorization == null ? "" : authorization.trim();
        log.info("Authorization: {}", authorization);

        if(!authorization.startsWith("Bearer "))
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, Constants.INVALID_TOKEN);

        final String token = authorization.substring(7).trim();

        try {
            final JwtClaims claims = jwtBuilder.generateParseToken(token);
            if (!claims.hasClaim("userID"))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            log.info("Autenticacion: userID = {}", claims.getClaimValue("userID"));

        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    private void preValidation(String idCustomer, String chargeAccountId, String destinationAccountId, Double amount) {
        if (isValid(idCustomer) || isValid(chargeAccountId) || isValid(destinationAccountId) || amount <= 0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constants.CAMPOS_INVALIDOS);
        if(!serviceCustomer.existsCustomer(idCustomer))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.CLIENT_NOT_FOUND);
        if(!serviceAccount.existsAccount(chargeAccountId))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.CHARGE_ACCOUNT_NOT_FOUND);
        if(!serviceAccount.existsAccount(destinationAccountId))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.DESTINATION_ACCOUNT_NOT_FOUND);
        Account chargeAccount = serviceAccount.getAccount(chargeAccountId);
        Account destinationAccount = serviceAccount.getAccount(destinationAccountId);
        if (!chargeAccount.getCustomerId().equals(idCustomer))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, Constants.CHARGE_ACCOUNT_NOT_FROM_CLIENT);
        if (!chargeAccount.getCurrency().equals(destinationAccount.getCurrency()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constants.DIFFERENT_CURRENCIES);
        if (chargeAccount.getBalance() < amount)
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, Constants.INSUFFICIENT_BALANCE);
    }

    private boolean isValid(String property) {
        return property == null || property.trim().isEmpty();
    }

}
